import SinglePage from "@/components/SinglePage";

export default function AboutPage() {
	return <SinglePage title="About Jooble" image="/images/passion.jpg" />;
}
