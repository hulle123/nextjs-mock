import React from "react";
import Link from "next/link";
import Image from "next/image";

import classes from "./SmallHeader.module.css";

export default function SmallHeader() {
	return (
		<section id={classes.intro}>
			<header id={classes.introHeader}>
				<h2>J</h2>
				<Link href="/" className={classes.logo}>
					<Image src="/images/logo.jpg" alt="logo" fill />
				</Link>
				<h2>bble</h2>
			</header>
			<header>
				<p>
					Bridging job and talent by{" "}
					<Link href="/referrer">me.</Link>
					<br />
				</p>
			</header>
		</section>
	);
}
