import React from "react";
import Image from "next/image";
import imgAvatar from "@/public/images/avatar.jpg";

import classes from "./SinglePage.module.css";

export default function SinglePage({ title, image }) {
	return (
		<main>
			<div id={classes.wrapper}>
				<article className="post">
					<header>
						<div className="title">
							<h2>
								<a href="#">{title}</a>
							</h2>
							<p>
								Lorem ipsum dolor amet nullam consequat etiam
								feugiat
							</p>
						</div>
						<div className="meta">
							<time className="published" dateTime="2015-11-01">
								April 20, 2024
							</time>
							<a href="#" className="author">
								<span className="name">Nikhil Hulle</span>
								<Image
									src={imgAvatar}
									width={40}
									height={40}
									alt="abc"
								/>
							</a>
						</div>
					</header>
					<span className={`${classes.image} ${classes.featured}`}>
						<Image src={image} alt="" fill />
					</span>
					<p>
					Marianne stood at the edge of the bustling market, her eyes scanning the vibrant stalls laden with fresh produce and colorful crafts. The aroma of roasted coffee mingled with the tangy scent of citrus fruits, 
					creating a lively atmosphere that energized her senses. She reached for a bright red apple, feeling its smooth skin against her fingertips, and smiled at the vendor, an elderly man with a kind, wrinkled face.
					</p>
					<p>
						Nearby, children played a game of tag, their laughter echoing through the air, weaving through the chatter of shoppers negotiating deals. Marianne&aposs heart warmed at the sight, reminding her of her own childhood spent in similar market squares. She continued walking, her steps rhythmic and sure, as she passed by artisans displaying their handmade jewelry and soft, woven scarves.

						As the sun climbed higher, casting long shadows across the cobblestones, Marianne paused to listen to a street musician strumming on an old guitar. His voice was raspy yet melodious, telling stories of distant lands and forgotten tales through his songs. She tossed a few coins into his open guitar case, nodded appreciatively, and moved on.	
					</p>
					<p>
					Turning down a quieter alley, she admired the small cafes and bistros lining the path, each offering a sanctuary for those looking to escape the midday sun. The smell of baking bread wafted from one open doorway, tempting Marianne to stop for a bite, but she decided to press on, her mind set on visiting the old bookstore at the end of the lane.

					Inside the bookstore, the air was cool and musty with the scent of paper and ink. Books of every shape and size filled the shelves, some stacked haphazardly, others lined up perfectly. Marianne ran her hands along the spines, feeling the embossed titles and worn edges. Each book held a universe of its own, promising endless possibilities and adventures.

					She finally selected a novel, a tale of mystery and intrigue set in a far-off land, and approached the counter. The bookseller, a middle-aged woman with spectacles sliding down her nose, looked up and smiled, her eyes twinkling behind the lenses. They exchanged pleasantries, discussing their favorite authors and recent reads, before Marianne bid her farewell, book in hand, ready to dive into another world as she stepped back into the sunlight.
					</p>
					<footer>
						<ul className="stats">
							<li>
								<a href="#">General</a>
							</li>
							<li>
								<a href="#" className="icon solid fa-heart">
									28
								</a>
							</li>
							<li>
								<a href="#" className="icon solid fa-comment">
									128
								</a>
							</li>
						</ul>
					</footer>
				</article>
			</div>
		</main>
	);
}
