
# JOOBLE

## NOTES

I have made another submission because I had already shown a demo of my application on the full-stack-app branch. Although I it covers 95% of the requirements, I wanted to show my willingness in creating another one and potray my willingness for the job. I have put significant effort into this and I hope you guys find it useful.

This task is another additional submission that I consider will play a major part when compared to my competition. I have focused on creating the major functionalities using the desired technologies in a most appropriate way.
I have focused on things that may have not completely satisfied by my full-stack-app branch like having various different meaningful pages, having a more a realistic use case base implementation. I have not implemented things like login because that has already been covered by my full-stack-app branch and also because of time constraints, I focused on potraying a wide array of skills as opposed to doing the same things over and over again. Due to time constraints, I might have missed out on implementing a thing or 2 but rest be assured this is a highly relevant product especially for compass health. I think this is what the focus of the company is at the present moment and this is a way to show that I can contrinute immensely.

## WebApp Details 

I consider myself well-versed in Node.js, with extensive experience both in implementing solutions and currently teaching the technology as a teaching assistant (https://cuboulder-csci3308.pages.dev/docs/labs/lab6) in a software development course at CU boulder. This task is designed to demonstrate my ability to go the extra mile and showcase how adeptly I can adopt new technologies and implement them effectively. This is a complete end to end implementation of a business use case, this project illustrates my proficiency with Next.js and my understanding of business requirements. To add to this I have also explored aws further by incorporating DynamoDB from AWS. For development and deployment purposes I made this project on a personal repo and moved this project to this repo, hence the less commits. I have already conveyed this to nipun (Just to be sure and cover all my tracks XD).


**Business Use Case: Job Connection Platform**

**Objective:** To streamline the job search and referral process by connecting job seekers and referrers on a single platform.

**Functionality:**
- **Secure User Profiles:** Users create profiles showcasing their skills and job preferences.
- **Job Listings:** Job seekers access and search through a database of job opportunities tailored to their needs.
- **Referrals:** Referrers can post special job postings of their organizations.

**Business Benefit:** This platform reduces the time and effort required to match job seekers with appropriate opportunities, enhancing recruitment efficiency and fostering professional networking.

### User Authentication and Authorization:

-  (To be Implemented) Implement a secure user authentication system allowing job seekers and referrers to register, log in, and manage their profiles.
-   Use authentication middleware to control access to certain features or data based on user roles (job seekers, referrers, admins).

### Job Listings and Search Functionality:

-   Develop a database schema to store job listings with details like job title, description, requirements, and company information.
-   Implement a search feature allowing job seekers to filter and find relevant job postings based on criteria like location, industry, or job type.


### Referral System:

-   Create a mechanism for referrers to post specific job listings within the platform.
-   (To be Implemented) Track and manage referrals, providing notifications and updates to referrers and job seekers on the status of their referrals.


## Technologies and Tools:

Frontend: HTML5, CSS3, JavaScript (Next.js)

Backend: Node.js 

Database: DynamoDB

Testing: Jest (unit), Supertest (integration), Cypress (end-to-end)

(Plan on using) Authentication: JWT (JSON Web Tokens), OAuth, Auth0 (TBD)

Version Control: Git, GitLab

Deployment: http://54.226.94.68/ 

I have deployed the complete application on ec2 in the interest of time. Like the previous full-stack-app, the app is deployed on ec2, the service is running in background in the ec2 machine with the help of systemctl and caddy. 
I have put significant efforts in creating this application especially working on my weaker skillsets like frontend.



